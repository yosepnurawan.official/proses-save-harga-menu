<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksipemesanan_model extends CI_Model
{
    /*
     * function construct dapat juga digunakan di model
     * berfungsi untuk memanggil data dari model lain 
     */
    public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
    }
    
    //panggil nama table
    private $_table = "transaksi_pemesanan";

    public function savePembelianDetail($id)
    {
        $pegawai            = $this->input->post('nik');
        $nama_pelanggan     = $this->input->post('nama_pelanggan');
        $kode_menu          = $this->input->post('kode_menu');
        $qty                = $this->input->post('qty');

        //cari harga menu dari model menu
        $hargamenu          = $this->menu_model->cariHargaMenu($kode_menu);

        // fungsi untuk melihat data yang dikirim / post
        // var_dump($hargamenu); die();
        
        $data['id_pembelian_h'] = $id;
        $data['nik']            = $kodebarang;
        $data['tgl_pemesanan']  = date('Y-m-d');
        $data['nama_pelanggan'] = $nama_pelanggan;
        $data['kode_menu']      = $kode_menu;
        $data['qty']            = $qty;
        // ini proses perkaliannya
        $data['total_harga']    = $qty * $hargamenu;

        $this->db->insert($this->_table, $data);
    }

}